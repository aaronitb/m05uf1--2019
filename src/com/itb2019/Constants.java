package com.itb2019;

public class Constants {
    // TODO: 25/11/19 canviar a IDIOMA_CATALA, IDIOMA_ESPANYOL, IDIOMA_INGLES
    public static final String IDIOMA_CATALA = "ca";
    public static final String IDIOMA_ESPANYOL = "es";
    public static final String IDIOMA_INGLES = "en";

    public static final String ESPAI = " ";
}
